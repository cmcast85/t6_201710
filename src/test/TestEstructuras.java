package test;

import model.data_structures.LinearProbingHashST;
import model.data_structures.SeparateHash;
import model.logic.GeneradorDatos;
import junit.framework.TestCase;

public class TestEstructuras extends TestCase{

	private SeparateHash<Integer, String> sh ;
	
	private LinearProbingHashST<Integer, String> lh;
	
	private GeneradorDatos gd;
	
	private String[] cadena;
	
	private int[] llaves;
	
	public void setUP(){
		sh = new SeparateHash<Integer,String >();
		lh = new LinearProbingHashST<Integer, String>();
	}
	
	public void setUP2(){
		sh = new SeparateHash<Integer,String >();
		lh = new LinearProbingHashST<Integer, String>();

		gd = new GeneradorDatos();
		
		cadena = gd.generarCadena(1000,10 );
		
		llaves = gd.generarAno(1000);
		
		for(int i = 0;i<1000;i++){
			sh.put(llaves[i], cadena[i]);
			lh.put(llaves[i], cadena[i]);
		}
		
		
	}
	
	public void testPut(){
		setUP();
		
		
		sh.put(1, "Prueba");
		sh.put(2, "p2");
		sh.put(3, "p3");
		
		assertTrue(sh.get(1).equals("Prueba"));
		assertTrue(sh.get(2).equals("p2"));
		assertTrue(sh.get(3).equals("p3"));
		
		
	}
	
	public void testLPut(){
		setUP();
		
		lh.put(1, "aj1");
		lh.put(2, "aj2");
		lh.put(3, "aj3");
		lh.put(4, "aj4");
		
		assertTrue(lh.get(1).equals("aj1"));
		assertTrue(lh.get(2).equals("aj2"));
		assertTrue(lh.get(3).equals("aj3"));
		assertTrue(lh.get(4).equals("aj4"));
	}
	
	public void testDelete(){
		setUP();
		
		sh.put(1, "Prueba");
		sh.put(2, "p2");
		sh.put(3, "p3");
		
		sh.delete(2);
		
		assertTrue(sh.get(2)==null);
		assertTrue(sh.get(1).equals("Prueba"));
	}
	
	public void testLDelete(){
		setUP();
		
		lh.put(1, "aj1");
		lh.put(2, "aj2");
		lh.put(3, "aj3");
		lh.put(4, "aj4");
		
		lh.delete(2);
		
		assertTrue(lh.get(2)==null);
		assertTrue(lh.get(4).equals("aj4"));
	}
	
	public void testSize(){
		setUP();
		
		sh.put(1, "Prueba");
		sh.put(2, "p2");
		sh.put(3, "p3");
		
		assertTrue(sh.size()==3);
		
		sh.delete(1);
		
		assertTrue(sh.size()==2);
	}
	
	public void testLSize(){
		setUP();
		
		lh.put(1, "aj1");
		lh.put(2, "aj2");
		lh.put(3, "aj3");
		lh.put(4, "aj4");
		
		assertTrue(lh.size()==4);
		
		lh.delete(1);
		
		assertTrue(lh.size()==3);
	}
	
	public void testContains(){
		setUP();
		
		sh.put(1, "Prueba");
		sh.put(2, "p2");
		sh.put(3, "p3");
		
		Iterable<Integer> st = sh.keys();
		int j = 1;
		for(Integer i : st){
			assertTrue(i==j);
			j++;
		}
		
	}
	
	public void testLContains(){

		setUP();
		
		lh.put(1, "aj1");
		lh.put(2, "aj2");
		lh.put(3, "aj3");
		lh.put(4, "aj4");
		
		Iterable<Integer> st = lh.keys();
		int j = 1;
		for(Integer i : st){
			assertTrue(i==j);
			j++;
		}
	}
	
}
