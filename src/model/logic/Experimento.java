package model.logic;

import model.data_structures.LinearProbingHashST;
import model.data_structures.SeparateHash;
import model.logic.StdDraw;

public class Experimento {

	
private SeparateHash<Integer, String> sh ;
	
	private LinearProbingHashST<Integer, String> lh;
	
	private GeneradorDatos gd;
	
	private String[] cadena;
	
	private int[] llaves;
	
	public Experimento(){
		sh = new SeparateHash<Integer,String >();
		lh = new LinearProbingHashST<Integer, String>();

		gd = new GeneradorDatos();
		
		cadena = gd.generarCadena(1000,10 );
		
		llaves = gd.generarAno(1000);
		
		for(int i = 0;i<1000;i++){
			sh.put(llaves[i], cadena[i]);
			lh.put(llaves[i], cadena[i]);
		}
	}
	
	
	// See Program 2.2.6.
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);       // number of coins
		int trials = Integer.parseInt(args[1]);  // number of trials
		
		// create the histogram
		Histogram histogram = new Histogram(n+1);
		for (int t = 0; t < trials; t++) {
			histogram.addDataPoint(Bernoulli.binomial(n));
		}
		
		// display using standard draw
		StdDraw.setCanvasSize(500, 100);
		histogram.draw();
	} 

	
	
}
