package model.logic;

public class Bernoulli { 

	    // number of heads when flipping n biased-p coins
	    public static int binomial(int n, double p) {
	        int heads = 0;
	        for (int i = 0; i < n; i++) {
	            if (StdRandom.bernoulli(p)) {
	                heads++;
	            }
	        }
	        return heads;
	    } 

	    // number of heads when flipping n fair coins
	    // or call binomial(n, 0.5)
	    public static int binomial(int n) {
	        int heads = 0;
	        for (int i = 0; i < n; i++) {
	            if (StdRandom.bernoulli(0.5)) {
	                heads++;
	            }
	        }
	        return heads;
	    }
	 
}
