package model.logic;

public class GeneradorDatos {

	
	public String[] generarCadena(int n,int k){
		String[] ret = new String[n];
		
		for(int i = 0; i<n ;i++){
			
			StringBuilder sb = new StringBuilder();
			int j = 0;
			while(j<k){
				int r = (int) Math.floor(Math.random() * ((90-65)+1) + 65);
				char c = (char)r;
				sb.append(c);
				j++;
			}
			String el = sb.toString();
			ret[i] = el;
		}
		return ret;
	}
	
	public int[] generarAno(int n){
		int ret[] = new int[n];
		
		for(int i = 0;i<n;i++){
			int r = (int) Math.floor(Math.random() * ((2000)+1) );
			
			
			ret[i] = r;
		}
		
		return ret;
	}
}
