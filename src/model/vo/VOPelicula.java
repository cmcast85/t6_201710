package model.vo;

public class VOPelicula {


	private String titulo;
	
	private int anio;
	
	private String director;
	
	private String actores;
	
	private double rating;
	
	public void setTitulo(String titulo){
		this.titulo = titulo;
	}
	
	public String getTitulo(){
		return titulo;
	}
	
	public void setAnio(int anio){
		this.anio = anio;
	}
	
	public int getAnio(){
		return anio;
	}
	
	public void setDirector(String director){
		this.director = director;
	}
	
	public String getDirector(){
		return director;
	}
	
	public void setActores(String actores){
		this.actores = actores;
	}
	
	public String getActores(){
		return actores;
	}
	
	public void setRating(double rating){
		this.rating = rating;
	}
	
	public double getRating(){
		return rating;
	}
}
